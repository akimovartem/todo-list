<?php

abstract class Model {

    public $db;

    public function __construct() {

        $this->db = new mysqli(HOST,USER,PASSWORD, DB);
    }

    public function getDb()
    {
        return $this->db;
    }

    public function normal_result($query){
        $result = $this->db->query($query);
        if(!$result) {
            exit(mysqli_error($result));
        }
        for($i = 0;$i < mysqli_num_rows($result); $i++) {
            $row[] = mysqli_fetch_array($result);
        }
        return $row;

    }

}