<?php

class Task extends Model{

    public function getTask($params){
        parse_str($params,$params);
        $query = "SELECT tasks.id AS id,tasks.name AS name,tasks.date,projects.name AS project_name ,projects.color AS project_color, priorities.color AS priority_color FROM `tasks` LEFT JOIN projects ON projects.id = tasks.project_id LEFT JOIN priorities ON priorities.id = tasks.priority_id   ";
        if(isset($params['complete']))
            $query.=" WHERE complete=1";
        else
            $query.=" WHERE complete=0";
        if(isset($params['id']))
            $query.= " AND tasks.id=".$params['id'];
        if(isset($params['project']))
            $query.=" AND tasks.project_id=".$params['project'];
        if(isset($params['period']))
            switch ($params['period']){
                case 'today': $query.=" AND tasks.date LIKE '".date("Y-m-d")."%'"; break;
                case 'week': $query.=" AND tasks.date <= '".date("Y-m-d", strtotime('+1 days'))."' AND tasks.date >= '".date("Y-m-d", strtotime('-7 days'))."'"; break;
            }

        $query.=" AND projects.user_id=".$_SESSION['user']['id']." ORDER BY date ASC, priority_id ASC";
        $result = $this->normal_result($query);
        return $result;
    }
    public function getAllTasks(){
        $query = "SELECT * FROM `tasks` WHERE `complete`=0";
        $result = $this->normal_result($query);
        return $result;
    }
    public function getTasks(){
        $query = "SELECT tasks.id AS id,tasks.name AS name,projects.name AS project_name ,projects.color AS project_color, priorities.color AS priority_color, tasks.date, projects.user_id FROM `tasks` LEFT JOIN projects ON projects.id = tasks.project_id LEFT JOIN priorities ON priorities.id = tasks.priority_id  WHERE complete=0 AND projects.user_id=".$_SESSION['user']['id'];
        $query.=" AND tasks.date <= '".date("Y-m-d", strtotime('+1 days')) ."' ORDER BY date ASC, priority_id ASC";
        $result = $this->normal_result($query);
        return $result;
    }
    public function addTask($params){
        parse_str($params,$params);
        $query = "INSERT INTO `tasks`(`name`, `project_id`, `priority_id`, `complete`, `date`) value ('".$params['name']."',".$params['project'].",".$params['priority'].",'0','".$params['date']."')";
        $result = $this->db->query($query);
        return true;
    }
    public function editTask($params){
        parse_str($params,$params);
        if(isset($params['complete']))
            $query = "UPDATE `tasks` SET complete='1' WHERE id =".$params['id'];
        else
            $query = "UPDATE `tasks` SET name='".$params['name']."',project_id=".$params['project'].",priority_id=".$params['priority'].",date='".$params['date']."' WHERE id =".$params['id'];
        $result = $this->db->query($query);
        return true;
    }
    public function removeTask($params){
        parse_str($params,$params);
        $query = "DELETE FROM `tasks` WHERE `id` =".$params['id'];
        $result = $this->db->query($query);
        return true;
    }
}