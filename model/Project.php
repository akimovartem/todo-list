<?php

class Project extends Model{

    public function getProjects(){
        $query = "SELECT projects.id,projects.color,projects.name,COUNT(tasks.project_id) AS `count` FROM `projects` LEFT JOIN tasks ON projects.id = tasks.project_id WHERE projects.user_id=".$_SESSION['user']['id']." GROUP BY projects.id";
        return $this->normal_result($query);
    }
    public function getProject($params){
        parse_str($params,$params);
        $query = "SELECT * FROM projects WHERE `id`=".$params['id'];
        return $this->normal_result($query);
    }
    public function addProject($params){
        parse_str($params,$params);
        $query = "INSERT INTO `projects`(`name`, `color`, `user_id`) value ('".$params['name']."','#".$params['color']."',".$_SESSION['user']['id'].")";
        $result = $this->db->query($query);
        return true;
    }
    public function editProject($params = null){
        parse_str($params,$params);
        $query = "UPDATE `projects` SET `name`='".$params['name']."',`color`='#".$params['color']."' WHERE `id` =".$params['id'];
        $result = $this->db->query($query);
        return true;
    }
    public function removeProject($params){
        parse_str($params,$params);
        $query = "DELETE FROM `projects` WHERE `id` =".$params['id'];
        $result = $this->db->query($query);
        return true;
    }
}