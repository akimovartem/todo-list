<?php

class RegistrationController extends ACore{

    public function post() {
        if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['re_password']) && isset($_POST['first_name']) && isset($_POST['last_name']))
        {
            if($_POST['password']!= $_POST['re_password'])
            {
                header("Location:/registration","" ,"Заполните обязательные поля");
            }
            $user=new User();
            if($user->existUser($_POST['email'])){
                header("Location:/registration");
            }
            if($user->addUser($_POST)){
                $user=$user->getUser($_POST['email']);
                $_SESSION['user'] = ['id'=>$user['id'],'first_name'=>$user['first_name']];
                 header("Location:/");
            }else{
                header("Location:/registration");
            }

        }
        else {
            header("Location:/registration");
        }
    }
    public function get($method){
        $this->get_body($method);
    }

    public function get_content() {

    }
}