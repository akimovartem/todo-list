<?php
class TaskController extends ACore {

    public function get($params=null){
        $task=new Task();

        if(isset($params) && $params!='task'){
            $task=$task->getTask($params);
        }else{
            $task = $task->getTasks();
        }
        echo json_encode($task);
    }
    public function post($params=null){
        $task=new Task();
        $task->addTask($params);
        return true;
    }
    public function put($params=null){
        $task=new Task();
        $task->editTask($params);
        return true;
    }
    public function delete($params=null){
        $task=new Task();
        $task->removeTask($params);
        return true;
    }
    function get_content()
    {
        // TODO: Implement get_content() method.
    }
}