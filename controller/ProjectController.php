<?php
class ProjectController extends ACore {

    public function get($params=null){
        $project=new Project();
        if(isset($params) && $params!='project'){

            $project=$project->getProject($params);
        }else{
            $project = $project->getProjects();
        }
        echo json_encode($project);
    }
    public function post($params=null){
        $project=new Project();
        $project->addProject($params);
        return true;
    }
    public function put($params=null){
        $project=new Project();
        $project->editProject($params);
        return true;
    }
    public function delete($params=null){
        $task=new Task();
        parse_str($params,$par);
        if(is_null($task->getTask('project='.$par['id']))){
            $project=new Project();
            $project->removeProject($params);
        }
        return true;
    }

    function get_content()
    {
        // TODO: Implement get_content() method.
    }
}