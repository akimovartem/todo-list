<?php

class LoginController extends ACore{


    public function post() {

		$login = strip_tags($_POST['login']);
		$password = strip_tags($_POST['password']);

		if(!empty($login) AND !empty($password)) {

			$user=new User();
			$user=$user->getUser ($login);

			if(!is_null($user) && password_verify($password,$user[0]['password'])) {
				$_SESSION['user'] = ['id'=>$user[0]['id'],'first_name'=>$user[0]['first_name']];
				header("Location:/");
				exit;
			}
			else {
                header("Location:/");
			}
		}
		else {
            header("Location:/");
		}
	}
    public function get($method){
        if(isset($_SESSION['user']))
            session_destroy();
        $this->get_body($method);
    }
	
	public function get_content() {
	}
}