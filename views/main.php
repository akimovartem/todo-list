<div class="col-xs-6 tasks-block">
    <h3>Today <span class="date"><?php echo date('D d M')?></span></h3>
    <ul id="list_tasks">

    </ul>
    <div><button class="btn-link btm-add-task">+ Add task</button></div>
    <div id="block_add_task" class="collapse">
        <div class="row">
            <div class="col-xs-9 margin-right-none">
                <input type="text" id="name_task" required>
            </div>
            <div class="col-xs-3 margin-left-none">
                <input type="text" id="date_task" required>
            </div>
        </div>
        <div>
            <button id="task_add" class="btn-link btn-custom btn-blue task-btn">Add</button>
            <button id="task_cancel" class="btn-link btn-custom">Cancel</button>

                <label class='dropdown toggle-right bolt'>
                    <span class='fa fa-bolt dropdown-toggle' type='button' data-toggle='dropdown'></span>
                    <ul class='dropdown-menu' id="priority_list">
                    </ul>
                </label>
            <label class='dropdown toggle-right smile'>
                <span class='fa fa-smile-o dropdown-toggle' type='button' data-toggle='dropdown'></span>
                <ul class='dropdown-menu' id="project_list">
                </ul></label>
        </div>
    </div>
</div>
</div>
<input id="priority_id" type="text" class="hidden">
<input id="project_id" type="text" class="hidden">
</div>
<script src="js/api.js"></script>
<script src="js/project.js"></script>
<script src="js/colorpicker.js"></script>
<script src="js/main.js"></script>
<script src="js/task.js"></script>
<script src="js/priority.js"></script>
<script src="js/jquery.datetimepicker.full.min.js"></script>