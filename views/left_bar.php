<div class="container">
    <div class="row">
        <div class="col-xs-3 project-main">
            <div class="project-block">
                <div><button id="today_task" class="btn-link btn-custom">Today</button></div>

                <div><button id="week_task" class="btn-link btn-custom">Next 7 days</button></div>

                <div class="div_project">Projects</div>
                <ul id="list_project">

                </ul>

                <div><button class="btn-link btm-add-project">+ Add project</button></div>
                <div id="block_change_projects" class="collapse">
                    <label class='span-project-color select-color-project'></label>
                    <input type="text" id="name_project" required>
                    <div>
                        <button id="project_add" class="btn-link btn-custom btn-width-50 btn-blue">Add</button><button id="project_cancel" class="btn-link btn-custom btn-width-50">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
        <input id="add_color_picker" class="hidden">
        <input id="edit_color_picker" class="hidden">



