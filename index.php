<?php
session_start();
header("Content-Type:text/html;charset=UTF-8");

require_once("config.php");

function __autoload($c) {

	if(file_exists("controller/".$c.".php")) {
		require_once "controller/".$c.".php";
	}
	elseif(file_exists("model/".$c.".php")) {
		require_once "model/".$c.".php";
	}
	
}

if(strpos($_SERVER['REQUEST_URI'],$_SERVER["HTTP_HOST"]))
    $length=strpos($_SERVER['REQUEST_URI'],$_SERVER["HTTP_HOST"])+strlen($_SERVER["HTTP_HOST"])+1;
else
    $length=1;
$method=substr($_SERVER['REQUEST_URI'],$length);
$param=false;

if(isset($_SESSION['user'])&&!$method){
        $method = 'main';
}else if (!isset($_SESSION['user'])){
    if ($method !== 'registration' && $method !== 'login')
        header("Location:/login");
}
if(strpos($method,'/')){
    $param=substr($method,strpos($method,'/')+1);
    $method=substr($method,0,strpos($method,'/'));
}

    if($method){
        $controller=strtoupper(mb_substr($method,0,1)).mb_substr($method,1)."Controller";
        $function=new $controller;
        $request = $_SERVER['REQUEST_METHOD'];
        switch ($request) {
            case 'GET':
                if ($param)
                    $function->get($param);
                else{
                    $param=$method;
                    $function->get($param);
                }
                break;
            case 'PUT':
                if ($param)
                    $function->put($param);
                else
                    exit('not params');
                break;
            case 'POST':
                $function->post($param);
                break;
            case 'DELETE':
                if ($param)
                    $function->delete($param);
                else
                    exit('not params');
                break;
        }


        //if(!isset($_POST) && !$_POST['method'])

    }
    else
        if(class_exists($class)) {
            $obj = new $class;
            $obj->get_body($class);
        }
        else {
            exit("<p>Нет данные для входа</p>");
        }

