-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 24 2017 г., 05:16
-- Версия сервера: 5.6.34
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `todo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `priorities`
--

CREATE TABLE `priorities` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `color` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `priorities`
--

INSERT INTO `priorities` (`id`, `name`, `color`) VALUES
(1, 'Hight', 'red'),
(2, 'Middle', 'orange'),
(3, 'Low', 'white');

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `color` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `name`, `color`, `user_id`) VALUES
(14, 'df1', '#00ff1e', 1),
(16, '333', '#ff0000', 1),
(17, '2333', '#ff0000', 1),
(18, 'dfdf', '#22d14b', 1),
(19, 'text akimov', '#ff0000', 12),
(20, 'testing', '#ff0000', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `complete` tinyint(1) NOT NULL,
  `date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `project_id`, `priority_id`, `complete`, `date`) VALUES
(1, 'testtask12333', 14, 1, 0, '0000-00-00 00:00:00'),
(2, 'testing', 15, 2, 1, '2017-10-24 00:58:48'),
(3, 'testing', 18, 3, 0, '0000-00-00 00:00:00'),
(5, 'rewr', 15, 2, 0, '0000-00-00 00:00:00'),
(6, 'test11111111111111111', 15, 2, 0, '0000-00-00 00:00:00'),
(7, 'test11111111111111111', 15, 3, 0, '0000-00-00 00:00:00'),
(9, 'qwerty', 15, 1, 0, '2017-10-24 06:17:00'),
(10, 'akimov', 19, 1, 0, '2017-10-24 08:37:00'),
(11, 'akimovs', 19, 1, 0, '2017-10-24 08:37:00'),
(12, 'akimovs', 19, 2, 0, '2017-10-24 08:37:00'),
(13, 'kjbkjbkb', 19, 1, 0, '2017-10-24 06:48:00'),
(15, 'kjbkb', 19, 2, 0, '2017-10-24 08:51:00'),
(16, 'test55555', 20, 2, 0, '2017-10-24 06:58:00');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `first_name` text COLLATE utf8_unicode_ci NOT NULL,
  `last_name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`) VALUES
(1, 'akimzp@mail.ru', '$2y$10$WplK55PpIJGtV3sn8fgIw.xWdg4fPUU2RtrrI08H0cC.pfdR.B4da', 'akimov', 'artem'),
(2, 'akimzp1@mail.ru', '$2y$10$oxRG9IQjc3tudk1fyaCoCuKmKDOCrnFuLZB5YVwqiwI15lw0.Y146', 'akimov', 'artem'),
(3, 'akimzp2@mail.ru', '$2y$10$9HBhzmLv5kbVKZdLTwB.neY/CCc4uKTLDSUSyg0fE5iE27gMOd5O.', 'akimov', 'artem'),
(4, 'akimzp21@mail.ru', '$2y$10$x82uljMlIrktlpJUJ0tnA.qDrJchk.IEnjCoMBf5V8XYfYQ5C8pLS', 'akimov', 'artem'),
(5, 'sdfd@wdwdw', '$2y$10$rUAhKzLmtzp8qJSug2KVeOSYI5Tud8AvuhKryZ958PmqQGuhNBAZm', 'akimov', 'dfsd'),
(6, 'akimzqqp@mail.ru', '$2y$10$p6OjJI.lu6Piidm5teDcxOLpqRuVdXHw8LWoNJBwO7YnQ8rf5qtBW', 'aw', 'we'),
(7, 'we@wew', '$2y$10$Ds8BlaVcQ0PTQxRL109g1.bh7V2gfpmAvTcBbsRdSsQaajznSiJpy', 'we', 'we'),
(8, '', '$2y$10$68nhlPHEnxp9HJZkZSbLDuZhxYJPckgGsLpK9J7db1kR1yN89wAoy', '', ''),
(9, '', '$2y$10$ekbn.HulI/iNp2Ex8jDIdeazDTRkxoyu0SWq/stTXR0WUgcxxVSgi', '', ''),
(10, '', '$2y$10$N7Z//vwekzY5xNrJ/j9hWON5HwrgPw91mDOSFTNb6fxpPSE4DRB3.', '', ''),
(11, '', '$2y$10$f2Dpn3qBAHg6OGftEJv4Mu97qhNzpuxGzyTcJ8ATvbZjkwMSwWywi', '', ''),
(12, 'akimovartem1992@gmail.com', '$2y$10$1fQZBkbJKqyPUJJCLVdpCeqQ199sbzPk5c3vMKrjOUncexwrImQxi', 'asdasdas', 'asdasda');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
