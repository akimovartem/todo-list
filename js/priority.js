var Priority = {
    getPriorityes: function(id) {
        API.call({
                type:'get',
                method:'priority'},
            function (result) {
                var str="";
                 $.each(result,function (index,item) {
                     if(id && id==item.id)
                         str+="<li data_id='"+item.id+"' class='sel'><label class='span-priority-color' style='background-color:"+item.color+"'></label>" +
                             "<label class='span-task-name'>"+item.name+"</label></li>";
                     else
                        str+="<li data_id='"+item.id+"'><label class='span-priority-color' style='background-color:"+item.color+"'></label>" +
                        "<label class='span-task-name'>"+item.name+"</label></li>";
                });
                $("#priority_list,#priority_list_edit").html(str);
            });
    }
}