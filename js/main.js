$(function() {
    //var data=""/*"id=5&name=test2&color=black"*/;
    Project.getProjects();
    Task.getTasks();
    Priority.getPriorityes();
    $('#task_add').click(function () {
        var name=$('#name_task').val();
        var date=$('#date_task').val();
        var project=$('#project_list .sel').attr('data_id');
        var priority=$('#priority_list .sel').attr('data_id');
        Task.addTask(name,date,project,priority);
    });
    $('#date_task').datetimepicker({dateFormat:'d-M-Y H:m',minuteInterval: 15});
    $("#project_list").on('click','li',function () {
        $("#project_list .sel").removeClass('sel');
        $(this).addClass('sel');
    });
    $("#arhive").click(function () {
        Task.getTasks(false,false,true);
    });
    $("#list_project").on('click','li',function () {
        $("#list_project .sel").removeClass('sel');
        $(this).addClass('sel')
        Task.getTasks($(this).attr('data_id'))
    });
    $("#list_tasks").on('click','#project_list_edit li',function () {
        $("#project_list_edit .sel").removeClass('sel');
        $(this).addClass('sel');
    });
    $("#priority_list").on('click','li',function () {
        $("#priority_list .sel").removeClass('sel');
        $(this).addClass('sel');
    });
    $("#today_task").click(function () {
        Task.getTasks(false,'today');
    });
    $("#week_task").click(function () {
        Task.getTasks(false,'week');
    });
    $("#list_tasks").on('click','#priority_list_edit li',function () {
        $("#priority_list_edit .sel").removeClass('sel');
        $(this).addClass('sel');
    });
    $(".div_project").click(function () {
        Project.getProjects();
        Task.getTasks();
    });
    $('#list_tasks').on('click','#task_edit_cancel',function () {
        $('#block_edit_task').remove();
    });
    $(".select-color-project").ColorPicker({
        onSubmit: function(hsb, hex, rgb, el) {
            $(".select-color-project").attr('style',  'background-color:#'+hex);
            $("#add_color_picker").val(hex);
            $(el).ColorPickerHide();
        }
    });
    $(".btm-add-project").click(function () {
        $('#block_change_projects').show();
        $(this).hide()
    });
    $(".btm-add-task").click(function () {
        $('#block_add_task').show();
        $(this).hide()
    });
    $("#project_cancel").click(function () {
        $(".btm-add-project").show();
        $('#block_change_projects').hide()
    });
    $("#task_cancel").click(function () {
        $(".btm-add-task").show();
        $('#block_add_task').hide()
    });

    $("#project_add").click(function () {
        var name=$("#name_project").val();
        var color=rgb2hex($(".select-color-project").css('background-color'));
        Project.addProject(name,color);
        $("#project_cancel").click();
    });;
    $('#list_project,#list_tasks').on('mouseenter','li',function () {
        $(this).children('.collapse').show();
    });
    $('#list_project,#list_tasks').on('mouseleave','li',function () {
        $(this).children('.collapse').hide();
    });
});
var hexDigits = new Array
("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");

//Function to convert rgb color to hex format
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return "" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}
