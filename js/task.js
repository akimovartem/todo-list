var Task = {
    getTasks: function(project,period,complete) {
        var method='task/';
        if(project)
            method+='project='+project;
        if(period)
            method+='&period='+period;
        if(complete)
            method+='&complete='+complete;

        API.call({
                type:'get',
                method:method},
            function (result) {
                var str="";
                var now = new Date();

                 $.each(result,function (index,item) {
                     var date_item = new Date(item.date);
                     if((now-date_item>0 || isNaN(now-date_item))&& !complete)
                         str+="<li class='warning'>";
                     else
                        str+="<li>";
                     str+=" <label class='span-priority-color' style='background-color:"+item.priority_color+"'></label>" +
                        "<label class='span-task-name'>"+item.name+"</label>" +
                        "<label class='dropdown collapse toggle-right'>"+
                        "<span class='glyphicon glyphicon-option-vertical dropdown-toggle' type='button' data-toggle='dropdown'></span>"+
                        " <ul class='dropdown-menu'>"+
                        " <li><a href='#' class='glyphicon glyphicon-pencil' aria-hidden='true' onclick='Task.showEditTask(this,"+item.id+")'></a></li>"+
                        "<li><a  href='#'class='glyphicon glyphicon-trash' aria-hidden='true' onclick='Task.removeTask("+item.id+")'></a></li>"+
                        "<li><a  href='#'class='glyphicon glyphicon-ok' aria-hidden='true' onclick='Task.editTask("+item.id+",true)'></a></li>"+
                        "</ul>"+
                        "</label><label class='span-task-project-color' style='background-color:"+item.project_color+"'></label>" +
                     " <div class='span-task-project-name'><span>"+item.project_name+"</span></div>";
                });
                $("#list_tasks").html(str);
            });
    },
    removeTask:function (id) {
        API.call({
                type:'delete',
                method:'task/id='+id},
            function (result) {
                if(result)
                    Task.getTasks();
                Project.getProjects();
            });
    },
    addTask:function (name,date,project,priority) {
        API.call({
                type:'post',
                method:'task/name='+name+"&date="+date+"&project="+project+"&priority="+priority},
            function (result) {
                if(result)
                    Task.getTasks();
                Project.getProjects();
            });
    },showEditTask:function (item,id) {
        $("#block_edit_task").remove();
        API.call({
                type:'get',
                method:'task/id='+id},
            function (result) {

                var parent = $(item).parent();
                parent = $(parent).parent();
                parent = $(parent).parent();
                parent = $(parent).parent();
                var children = "<div id='block_edit_task' class=''>" +
                    "<div id='block_edit_task'><div class='row'><div class='col-xs-9 margin-right-none'> <input type='text' id='name_task_edit' value='"+result[0].name+"' required> </div>"+
                    "<div class='col-xs-3 margin-left-none'><input type='text' id='date_task_edit' value='"+result[0].date+"' required> </div> </div> <div>"+
                    "<button class='btn-link btn-custom btn-blue task-btn' onclick='Task.editTask("+result[0].id+")'>Change</button><button id='task_edit_cancel' class='btn-link btn-custom'>Cancel</button>"+
                    "<label class='dropdown toggle-right bolt'>"+
                    "<span class='fa fa-bolt dropdown-toggle' type='button' data-toggle='dropdown'></span><ul class='dropdown-menu' id='priority_list_edit'> </ul> </label>"+
                    "<label class='dropdown toggle-right smile'> <span class='fa fa-smile-o dropdown-toggle' type='button' data-toggle='dropdown'></span> <ul class='dropdown-menu' id='project_list_edit'> </ul></label> </div> </div>";
                $(parent).append(children);
                Priority.getPriorityes(result[0].priority_id);
                Project.getProjects(result[0].project_id);
                $("#date_task_edit").datetimepicker({dateFormat:'d-M-Y H:m',minuteInterval: 15});

            });
    },
    editTask:function (id,complete) {
        var name=$('#name_task_edit').val();
        var date=$('#name_task_edit').val();
        var project=$('#project_list_edit .sel').attr('data_id');
        var priority=$('#priority_list_edit .sel').attr('data_id');
        if(complete)
            var method='task/id='+id+'&complete='+complete;
        else
            var method='task/id='+id+'&name='+name+"&date="+date+"&project="+project+"&priority="+priority;
        API.call({
                type:'put',
                method:method},
            function (result) {
                if(result)
                    Task.getTasks();
                Project.getProjects();
                $("#block_edit_task").remove();
            });
    }
}