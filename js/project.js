var Project = {
    getProjects: function(id) {
        API.call({
                type:'get',
                method:'project'},
            function (result) {
                var str="",str2="";
                $.each(result,function (index,item) {
                    str+="<li data_id='"+item.id+"'><label class='span-project-color' style='background-color:"+item.color+"'></label>" +
                        " <label class='span-name-project'>"+item.name+"</label><label class='span-count-project'>"+item.count+"</label>" +
                        "<label class='dropdown collapse'>"+
                        "<span class='glyphicon glyphicon-option-vertical dropdown-toggle' type='button' data-toggle='dropdown'></span>"+
                        " <ul class='dropdown-menu'>"+
                        " <li><a href='#' class='glyphicon glyphicon-pencil' aria-hidden='true' onclick='Project.showEditProject(this,"+item.id+")'></a></li>"+
                        "<li><a  href='#'class='glyphicon glyphicon-trash' aria-hidden='true' onclick='Project.removeProject("+item.id+")'></a></li>"+
                        "</ul>"+
                        "</label>";
                    if(id && id==item.id)
                        str2+="<li data_id='"+item.id+"' class='sel'><label class='span-project-color' style='background-color:"+item.color+"'></label>" +
                            " <label class='span-name-project'>"+item.name+"</label></li>";
                    else
                    str2+="<li data_id='"+item.id+"'><label class='span-project-color' style='background-color:"+item.color+"'></label>" +
                        " <label class='span-name-project'>"+item.name+"</label></li>";
                });
                $("#list_project").html(str);
                $("#project_list,#project_list_edit").html(str2);
            });
    },
    addProject: function(name,color) {
        API.call({
                type:'post',
                method:'project/name='+name+'&color='+color},
            function (result) {
            if(result)
               Project.getProjects();
            });
    },
    removeProject: function(id) {
        API.call({
                type:'delete',
                method:'project/id='+id},
            function (result) {
                if(result)
                    Project.getProjects();
            });
    },
    showEditProject:function (item,id) {
        $("#block_edit_project").remove();
        API.call({
                type:'get',
                method:'project/id='+id},
            function (result) {

                var parent = $(item).parent();
                parent = $(parent).parent();
                parent = $(parent).parent();
                parent = $(parent).parent();
                var children = "<div id='block_edit_project' class=''>" +
                    "<label id='edit_color_project' class='span-project-color select-color-project' style='background-color:"+result[0].color+" '></label>" +
                    "<input type='text' id='edit_name_project' value='"+result[0].name+"' required>" +
                    "<div>" +
                    "<button class='btn-link btn-custom btn-width-50 btn-blue' onclick='Project.editProject("+result[0].id+")'>change</button>" +
                    "<button class='btn-link btn-custom btn-width-50' onclick='Project.closeProjectEdit(this)'>Cancel</button>" +
                    "</div></div>";
                $(parent).append(children);
                $("#edit_color_project").ColorPicker({
                    onSubmit: function(hsb, hex, rgb, el) {
                        $("#edit_color_project").attr('style',  'background-color:#'+hex);
                        $("#edit_color_picker").val(hex);
                        $(el).ColorPickerHide();
                    }
                });
                console.log(result[0].color);
                $("#edit_color_picker").val( result[0].color.substr(1));
            });

    },
    closeProjectEdit:function (item) {
        var parent = $(item).parent();
        parent = $(parent).parent();
        $(parent).remove();
    },
    editProject:function (id) {
        console.log($("#edit_color_picker").val());
        var color=$("#edit_color_picker").val();
        var name=$("#edit_name_project").val();
        API.call({
                type:'put',
                method:'project/id='+id+'&name='+name+'&color='+color},
            function (result) {
                if(result)
                    Project.getProjects();
            });
    }
};