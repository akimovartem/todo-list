var API = {
	/**
	 * Performs an API call.
	 * Does not use header credentials because is intended to be used only by authenticated users.
	 *
	 * @param params.type String "GET" or "POST",
	 * @param params.method String the API method
	 * @param params.data Array the POST data
	 * @param callback Function the callback after the response
	 */

	call: function(params,callback) {
		if(params.params)
			params.method+='/'+params.params;
		switch(params.type) {
			case "get":
				$.ajax({
					type: "get",
					url: "/" + params.method,
					success: function(response) {
						if(response){
                            response=JSON.parse(response);
                            callback(response);
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log('error');
					}
				});
				break;
			case "post":
				$.ajax({
					type: "post",
					url: "/"+params.method,
					success: function(response) {
                        if(response){
                            response=JSON.parse(response);
                            callback(response);
                        }else {
                            callback(true);
                        }
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log('error');
					}
				});
				break;
            case "put":
                $.ajax({
                    type: "put",
                    url: "/"+params.method,
                    success: function(response) {
                        if(response){
                            response=JSON.parse(response);
                            callback(response);
                        }else {
                        	callback(true);
						}
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('error');
                    }
                });
                break;
            case "delete":
                $.ajax({
                    type: "delete",
                    url: "/"+params.method,
                    success: function(response) {
                        if(response){
                            response=JSON.parse(response);
                            callback(response);
                        }else {
                            callback(true);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('error');
                    }
                });
                break;
		}
	}
}